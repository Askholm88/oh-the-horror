import React, { useState } from "react"
import {
    Button,
    StyleSheet,
    Text,
    View,
    TextInput,
    ScrollView,
    ImageBackground,
    FlatList,
    TouchableOpacity,
  } from "react-native"
import styled from "styled-components"


// assets
import HeaderBackground from "../assets/header.jpg"


const StyledView = styled(View)`
  padding: 20px;
  width: 100%;
`

const StyledText = styled(Text)`
  color: black;
  font-weight: bold;
  text-align: center;
  padding-bottom: 10px;
`


export default function Header() {
    const [name, setName] = useState("Charley")
/*     const [person, setPerson] = useState({
      game: "Silent Hill 1",
      name: "Harry Mason",
      age: 50,
    }) */

    return(
        <>
          <ImageBackground source={HeaderBackground} style={styles.image} />
            <StyledView>
              <StyledText>Hello {name}</StyledText>
              <Text>
                Welcome to oh The Horror hobby project app developed in React Native. Feel free to explore and add new games to the list
              </Text>
            </StyledView>
            </>
    )}

    const styles = StyleSheet.create({
        container: {
          flex: 1,
          backgroundColor: "#fff",
          alignItems: "center",
          paddingTop: 40,
          //justifyContent: "center",
        },
        image: {
          flex: 1,
          resizeMode: "cover",
          justifyContent: "center",
          width: "100%",
          height: 200,
        },
      })