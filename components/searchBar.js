import React, { useState } from "react"
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Button,
  TextInput,
} from "react-native"
import Search from '../services/searchService'
import styled from "styled-components"

const StyledInput = styled(TextInput)`
  border: 1px black solid;
  border-radius: 5px;
  width: 50%;
`



export default function SearchBar() {
    const [query, setQuery] = useState("")

    const handleSearch = (value) => {
        setQuery(() => (value))  
      }

    const handlePress = (queryString) => {
        Search(queryString)
    }
    
      console.log(query)
  return (
    <>
      <Text>Search for game</Text>
      <StyledInput
        placeholder="e.g. Resident Evil 1"
        onChangeText={(value) => handleSearch(value)}
      />
      <Button title="Search" onPress={() => handlePress(query)} />
    </>
  )
}
