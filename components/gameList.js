import React, { useState } from "react"
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Button,
  TextInput,
} from "react-native"
import styled from "styled-components"

// assets

const StyledList = styled(View)`
  width: 95%;
  margin-top: 25px;
  display: flex;
`

const Game = styled(View)`
  width: 98%;
  display: flex;
  justify-content: flex-end;
  background: gray;
  padding: 20px;
  border: black 1px solid;
  margin: 5px;
`

const GameText = styled(Text)`
  background: gray;
`

const ButtonContainer = styled(View)`
  margin-top: 25px;
`

const StyledInput = styled(TextInput)`
  border: 1px black solid;
  border-radius: 5px;
  width: 50%;
`

const Space = styled(View)`
  width: 100%;
  height: 25px;
`

const FormContainer = styled(View)`
  display: flex;
  align-items: center;
  flex: 1;
`

export default function GameList({message}) {
  const [games, setGames] = useState([
    { game: "Layers of Fear 2", rating: 1 },
    { game: "Outlast", rating: 10 },
    { game: "Amnesia - Rebirth", rating: 6 },
    { game: "The Last of Us 2", rating: 7 },
  ])
  const [newGame, setNewGame] = useState({ game: "", rating: 0 })

  const handleSearch = (value) => {
    setNewGame((prev) => ({ ...prev, game: value }))

  }


  const clickHandlerThree = () => {
    setGames((prev) => [
      ...games,
      { game: newGame.game, rating: newGame.rating },
    ])
  }

  const pressHandler = (game) => {
    console.log(game)
    setGames((prevGames) => {
      return prevGames.filter((current) => current.game != game)
    })
  }

  return (
    <StyledList>
      <FormContainer>
      
        <Text>Enter name of game: {message}</Text>
        <StyledInput
          placeholder="e.g. Resident Evil 1"
          onChangeText={(value) =>
            setNewGame((prev) => ({ ...prev, game: value }))
          }
        />
        <Text>Enter rating:</Text>
        <StyledInput
          keyboardType="numeric"
          placeholder="enter number from 1-10"
          onChangeText={(value) =>
            setNewGame((prev) => ({ ...prev, rating: value }))
          }
        />
      </FormContainer>
      <Space />
      <Button title="add game" onPress={clickHandlerThree} />
      <Space />
      {games.map((currentGame, index) => (
        <TouchableOpacity
          key={index + currentGame.game}
          onPress={() => pressHandler(currentGame.game)}
        >
          <Game>
            <GameText>Name: {currentGame.game}</GameText>
            <GameText>Rating: {currentGame.rating}</GameText>
          </Game>
        </TouchableOpacity>
      ))}
    </StyledList>
  )
}

const styles = StyleSheet.create({})
