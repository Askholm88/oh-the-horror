import axios from "axios";

const options = (queryString) => ({
        method: 'GET',
        url: `https://imdb-internet-movie-database-unofficial.p.rapidapi.com/search/${queryString}`,
        headers: {
            'x-rapidapi-key': 'fff8a54958mshe03c2309467b62dp17b660jsn06f6a478894c',
            'x-rapidapi-host': 'imdb-internet-movie-database-unofficial.p.rapidapi.com'
        }
    })
  

  export default function Search(queryString) {

      axios.request(options(queryString)).then(function (response) {
          console.log(response.data);
        }).catch(function (error) {
            console.error(error);
        });
    }