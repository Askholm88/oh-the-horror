import { StatusBar } from "expo-status-bar"
import React, { useState } from "react"
import styled from "styled-components"
import {
  Button,
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  FlatList,
} from "react-native"

// components
import GameList from "./components/gameList"
import Header from "./components/header"
import SearchBar from "./components/searchBar"

// assets

const Bodytext = styled(Text)`
  text-align: center;
  font-size: 30px;
`

const Body = styled(View)`
  padding: 20px;
  width: 100%;
  border: 1px grey;
  background: grey;
  border-bottom-width: 2px;
  border-bottom-color: black;
`

const Space = styled(View)`
  width: 100%;
  height: 25px;
`

export default function App() {

  const clickHandler = () => {
    if (name === "Charley") {
      setName("Hans!!!!")
    } else {
      setName("Charley")
    }
  }

  const clickHandlerTwo = () => {
    if (person.name === "Harry Mason") {
      setPerson({ game: "Siren 1", name: "Yoshi", age: 17 })
    } else {
      setPerson({ game: "Silent Hill 1", name: "Harry Mason", age: 50 })
    }
  }

  return (
    <>
      <ScrollView>
        <View style={styles.container}>
          <Header />
          <Space />
          <Body><Bodytext>Games list</Bodytext></Body>
          <Space />
          <SearchBar />
         {/*  <ButtonContainer>
            <Button title="change you name here!" onPress={clickHandler} />
            <Space />
            <Button
              title="change character info here!"
              onPress={clickHandlerTwo}
            />
            <Space />
          </ButtonContainer> */}
          <GameList/>
          <Space />
        </View>
      </ScrollView>
      {/* Trying out with flatList component (better for performance on big lists) */}
      {/* <FlatList
        keyExtractor={(item) => item.game}
        numColumns={2}
        data={games}
        renderItem={({ item }) => (
          <>
            <GameText>{item.game}</GameText>
            <GameText>{item.rating}</GameText>
          </>
        )}
      /> */}
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    paddingTop: 40,
    //justifyContent: "center",
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    width: "100%",
    height: 200,
  },
})
